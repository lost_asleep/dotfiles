if [ -f ~/.bashrc ]; then
    source ~/.bashrc
fi

##
# Your previous /Users/phil/.bash_profile file was backed up as /Users/phil/.bash_profile.macports-saved_2021-05-06_at_17:42:38
##

# MacPorts Installer addition on 2021-05-06_at_17:42:38: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

