" designed for vim 7+
let skip_defaults_vim=1
set nocompatible

set encoding=utf-8    " to support german Umlaute
scriptencoding utf-8
set tw=0 " max line length = 0, so now auto newline chars


"####################### Vi Compatible (~/.exrc) #######################

" automatically indent new lines
set autoindent

" automatically write files when changing when multiple files open
set autowrite

" activate line numbers
set number

" turn col and row position on in bottom right
set ruler " see ruf for formatting

" show command and insert mode
set showmode

set tabstop=2


"#######################################################################

set norelativenumber        " nice, but not for beginners and streaming
set softtabstop=2
set shiftwidth=2
set smartindent
set smarttab


if v:version >= 800
  " stop vim from silently fucking with files that it shouldn't
  set nofixendofline

  " better ascii friendly listchars
  set listchars=space:*,trail:*,nbsp:*,extends:>,precedes:<,tab:\|>

  " i fucking hate automatic folding
  set foldmethod=manual
  set nofoldenable
endif


" mark trailing spaces as errors
match ErrorMsg '\s\+$'

" replace tabs with spaces automatically
set expandtab

" prevents truncated yanks, deletes, etc (important!)
" increases the maximum buffer size so temp data will not be lost
set viminfo='20,<1000,s1000

" requires PLATFORM env variable set (in ~/.bashrc)
if $PLATFORM == 'mac'
    " required for mac.delete to work"
    set backspace=indent,eol,start
endif

" command history
set history=100

" here because plugins and stuff need it
syntax enable

" disable the weird search highlights
set nohlsearch

" faster scrolling
set ttyfast

" allow sensing the filetype
filetype plugin on

" set the background to dark
set bg=dark

" disable the weird search highlights
set nohlsearch

" replace tabs with spaces automatically
set expandtab

" prevent the cursor from moving off screen
set scrolloff=2

" keep the terminal title updated
set laststatus=0
set icon


"#######################################################################
" Python specific settings upon loading *.py files. Taken from:
" https://realpython.com/vim-and-python-a-match-made-in-heaven/
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

" also folding from the same article
set foldmethod=indent
set foldlevel=99


"#######################################################################

" Prevent automatic newline characters in txt files on windows
autocmd BufRead,BufNewFile   *.txt setlocal formatoptions-=t formatoptions+=croql


"#######################################################################

" configure backup to one central folder
" Check if the .vimtmp folder exists in $HOME. If not it will be created
if !isdirectory($HOME."/.vimtmp")
    call mkdir($HOME."/.vimtmp", "", 0770)
endif

set backupdir=$HOME/.vimtmp//,. " for backup files
set directory=$HOME/.vimtmp//,. " for swap files
set undodir=$HOME/.vimtmp//,. " for undo files
" The double slash at the end ensures that there is no conflict in case of two files
" having the same name (maybe only honored for swap files)
" The ,. allow vim to use the current directory if the former doesn't exist.

set undofile " enable undo files
set backup " enable backup (and swap)


"#######################################################################

" Install vim-plug if not already installed. According to rwxrob plug
" is better and Vim 8 Plugins suck
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
  echo "Don't forget to GoInstallBinaries if you're doing Go dev."
endif

" only load plugins if Plug detected
if filereadable(expand("~/.vim/autoload/plug.vim"))
  call plug#begin('~/.vimplugins')
  Plug 'vim-pandoc/vim-pandoc'
  Plug 'vim-pandoc/vim-pandoc-syntax'

  " Taken from: https://www.vimfromscratch.com/articles/vim-for-python/
  Plug 'tpope/vim-commentary'
  Plug 'jeetsukumaran/vim-pythonsense'
  Plug 'jiangmiao/auto-pairs'
  Plug 'sheerun/vim-polyglot'

  "Plug 'vim-scripts/indentpython.vim'  " includes skipped line indents
  "Plug 'vim-syntastic/syntastic'  " Check Python3 Syntax on save
  "Plug 'nvie/vim-flake8'  " Add PEP 8 checking for Python3
  Plug 'davidhalter/jedi-vim' " the jedi completion engine for Python
  Plug 'lifepillar/vim-mucomplete' " Minimalist autocomplete popup
  Plug 'lifepillar/vim-gruvbox8' " An optimized gruvbox colorscheme
  Plug 'fatih/molokai'
  call plug#end()
endif

" colorscheme gruvbox8 " set the colorscheme (now installed)
colorscheme molokai


" enable omni-completion, needs filetype plugin on
set omnifunc=syntaxcomplete#Complete

" Make the Python code look pretty
let python_highlight_all=1
syntax on

" Settings for vim-jedi (Python)
"let g:jedi#goto_command = "<leader>d"
"let g:jedi#goto_assignments_command = "<leader>g"
"let g:jedi#goto_stubs_command = "<leader>s"
"let g:jedi#goto_definitions_command = ""
"let g:jedi#documentation_command = "K"
"let g:jedi#usages_command = "<leader>n"
"let g:jedi#completions_command = "<C-Space>"
"let g:jedi#rename_command = "<leader>r"

" Settings for the mucomplete plugin
set completeopt+=menuone
set completeopt+=noinsert
set shortmess+=c  " Shut off completion messages
set belloff+=ctrlg " If Vim beebs during completion
let g:mucomplete#enable_auto_at_startup = 1  "autostart
let g:mucomplete#completion_delay = 1


"#######################################################################

" From the defaul vimrc example file. When started as "evim", evim.vim
" evim.vim will already have done these settings, bail out.
if v:progname =~? "evim"
  finish
endif

" The matchit plugin makes the % command work better, but is not backwards
" compatible. The ! means the package will only load during plugin init.
if has('syntax') && has('eval')
  packadd! matchit
endif

